# produces an output value named "space_heroes"
output "pokimon" {
  description = "API that documents folks in space"
  value       = jsondecode(data.http.poki.response_body)
}
